# README #

This README would normally document whatever steps are necessary to get your application up and running.

### What is this repository for? ###

* Post process NLP augmeneted data so that the quality of the data improves to use it in the downstream tasks.
* 1.0

### How do I get set up? ###

* Python > 3.7 , latest and trasformer libraries are required.
* !pip install torch
* !pip install transformers
* !pip install sentencepiece
* !pip install sentence-transformers
* !pip install pandas
* !pip install statistics
* !pip install numpy
* all other basic python packaes
* Good internet connectivity to download the finetuned and pretrained models
* Once cloned and installed required packages, ensure the models are in the same level directory. 
* run python inference.py -i augmentations.json -o augmentation_results.json
* -i input file having augmented.json and -o the output file to write the post processed results